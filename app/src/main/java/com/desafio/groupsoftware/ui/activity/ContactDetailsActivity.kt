package com.desafio.groupsoftware.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.ui.controller.ContactDetailsController
import com.desafio.groupsoftware.ui.extras.USER_EXTRA_KEY
import kotlinx.android.synthetic.main.activity_contact_details.*

class ContactDetailsActivity : AppCompatActivity() {


    lateinit var user: User
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        ViewCompat.setTransitionName(contact_image_profile_contact_details, VIEW_HEADER_IMAGE)

        intentCheckKey()
    }


    private fun intentCheckKey() {

        if (intent.hasExtra(USER_EXTRA_KEY)) {
            this.user = intent.getParcelableExtra(USER_EXTRA_KEY) as User
        }
    }

    override fun onResume() {
        super.onResume()
        ContactDetailsController(this)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    companion object {
        const val VIEW_HEADER_IMAGE = "profile_image"
    }
}
