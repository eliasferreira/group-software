package com.desafio.groupsoftware.ui.controller

import android.annotation.SuppressLint
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.app.BaseApplication
import com.desafio.groupsoftware.model.Phone
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.ui.activity.ContactDetailsActivity
import com.desafio.groupsoftware.ui.activity.ContactFormActivity
import com.desafio.groupsoftware.ui.adapter.UserPhoneAdapter
import kotlinx.android.synthetic.main.activity_contact_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactDetailsController(val contactDetailsActivity: ContactDetailsActivity) {

    private var user: User? = null
    private val baseApplication: BaseApplication =
        contactDetailsActivity.application as BaseApplication
    private val contactRepository = baseApplication.getUserRepository()


    init {
        getUserData()

    }

    private fun getUserData() {

        CoroutineScope(Dispatchers.IO).launch {
            user = contactDetailsActivity.user.id?.let { contactRepository.findContact(it) }
            if (user != null) {
                contactDetailsActivity.runOnUiThread {

                    contactDetailsActivity.supportActionBar?.title =
                        "${user!!.username} ${user!!.middleName}".replace("null", "")
                    applayContactDataOnView()
                    contactDetailsActivity.contact_update_button.setOnClickListener {
                        startContactUpdateAction(user = user!!)
                    }
                }
                getUserPhones()
            }
        }
    }

    private fun getUserPhones() {
        user?.id?.let {
            CoroutineScope(Dispatchers.IO).launch {
                val contactPhones = contactRepository.getContactPhones(it)
                    if (contactPhones.isNotEmpty()) {
                        contactDetailsActivity.runOnUiThread {
                            contactDetailsActivity.recycler_view_contact_phones_detail.apply {
                                layoutManager =
                                    LinearLayoutManager(
                                        contactDetailsActivity,
                                        RecyclerView.VERTICAL,
                                        false
                                    )
                                adapter = UserPhoneAdapter(
                                    contactDetailsActivity,
                                    contactPhones.sorted()  as MutableList<Phone>,
                                    true
                                )
                            }
                        }
                    }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun applayContactDataOnView() {

        if (user?.cpf != null) {
            contactDetailsActivity.user_cpf.text =
                "${contactDetailsActivity.getString(R.string.user_cpf)}: ${user?.cpf}"
        }

        contactDetailsActivity.user_email.text = "Email: ${user?.email}"

        Glide.with(contactDetailsActivity)
            .load(user?.image)
            .apply(
                RequestOptions
                    .centerInsideTransform()
                    .skipMemoryCache(false)
                    .error(R.drawable.img_not_found)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
            ).into(contactDetailsActivity.contact_image_profile_contact_details)
    }

    private fun startContactUpdateAction(user: User) {
        contactRepository.user = user
        val intent =
            Intent(contactDetailsActivity, ContactFormActivity::class.java)
        contactDetailsActivity.startActivity(intent)
    }
}