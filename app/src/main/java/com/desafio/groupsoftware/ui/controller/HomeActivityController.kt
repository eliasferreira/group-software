package com.desafio.groupsoftware.ui.controller

import android.content.Intent
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.app.BaseApplication
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.ui.action.ScrollAwareViewBehavior
import com.desafio.groupsoftware.ui.activity.HomeActivity
import com.desafio.groupsoftware.ui.activity.ContactFormActivity
import com.desafio.groupsoftware.ui.adapter.ContactAdapter
import com.desafio.groupsoftware.ui.viewmodel.ContactsViewModel
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivityController(val homeActivity: HomeActivity) {

    private var baseApplication: BaseApplication = homeActivity.application as BaseApplication
    private var contactRepository = baseApplication.getUserRepository()
    private var usersList: List<User> = ArrayList()
    private lateinit var contactAdapter: ContactAdapter

    private lateinit var contactsViewModel: ContactsViewModel
    private var usersRecyclerView = homeActivity.recycler_view_contatos


    init {
        addContactAction()
        contactsViewModelStart()
    }


    private fun addContactAction() {
        homeActivity.user_add_button.setOnClickListener {
            val intent = Intent(homeActivity, ContactFormActivity::class.java)
            homeActivity.startActivity(intent)
        }
    }

    /** Observation of contact list on update */
    private fun contactsViewModelStart() {
        contactsViewModel =
            ViewModelProviders.of(homeActivity).get(ContactsViewModel::class.java)
        contactsViewModel.start(contactRepository)


        contactsViewModel.getUser().observe(homeActivity, Observer { usersList ->
            this.usersList = usersList.sorted()
            recyclerViewInitializer()
        })
    }

    /** RecyclerView of contact list initialization */
    private fun recyclerViewInitializer() {
        contactAdapter = ContactAdapter(contactRepsitory = contactRepository, usersList = usersList, activity = homeActivity)
        usersRecyclerView.apply {
            layoutManager = LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false)
            adapter = contactAdapter
        }

        fabAnimation()
    }

    /** Animation FAB contact add button on RecyclerView scrooling */
    private fun fabAnimation() {
        val animationHide = AnimationUtils.loadAnimation(homeActivity, R.anim.hide_to_botton)
        val animationShow =
            AnimationUtils.loadAnimation(homeActivity, R.anim.reset_botton_to_oginal_position)

        ScrollAwareViewBehavior(
            view = homeActivity.user_add_button,
            recyclerView = usersRecyclerView,
            animateOut = animationHide,
            animateIn = animationShow
        ).start()
    }
}