package com.desafio.groupsoftware.ui.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.model.Phone
import com.softwareplace.workout.ui.permission.callPhoneRequestUserPermission
import kotlinx.android.synthetic.main.user_phones_layout.view.*


class UserPhoneAdapter(
    val activity: Activity,
    var phonesList: MutableList<Phone>,
    val fullDetail: Boolean

) : RecyclerView.Adapter<UserPhoneAdapter.ViewHolder>() {

    var phoneToBeDeleted = arrayListOf<Phone>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (fullDetail) {
            false -> {
                return ViewHolder(
                    userPhoneAdapter = this,
                    itemView = LayoutInflater.from(parent.context).inflate(
                        R.layout.user_phones_layout,
                        parent,
                        false
                    ),
                    activity = activity
                )
            }
            else -> {
                return ViewHolder(
                    itemView = LayoutInflater.from(parent.context).inflate(
                        R.layout.user_phones_layout_detail,
                        parent,
                        false
                    ),
                    userPhoneAdapter = this,
                    activity = activity
                )
            }
        }

    }


    override fun getItemCount(): Int {
        return this.phonesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(phone = phonesList[position])
    }


    class ViewHolder(
        val userPhoneAdapter: UserPhoneAdapter,
        itemView: View,
        val activity: Activity
    ) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(phone: Phone) {

            itemView.user_phone_number.text = phone.number
            itemView.user_phone_modality.text = phone.modality

            if (!userPhoneAdapter.fullDetail) {
                val delete = itemView.phone_delete

                delete.setOnClickListener {
                    userPhoneAdapter.phoneToBeDeleted.add(phone)
                    userPhoneAdapter.phonesList.remove(phone)
                    userPhoneAdapter.notifyDataSetChanged()
                }

            } else {
                itemView.setOnClickListener {
                    callPhoneRequestUserPermission(activity = activity, phoneNumber = phone.number)
                }
            }
        }
    }
}