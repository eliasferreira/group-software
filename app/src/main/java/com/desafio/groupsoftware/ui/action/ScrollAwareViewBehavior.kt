package com.desafio.groupsoftware.ui.action


import android.view.View
import android.view.animation.Animation
import androidx.recyclerview.widget.RecyclerView

class ScrollAwareViewBehavior(
        val recyclerView: RecyclerView,
        val view: View,
        val animateIn: Animation,
        val animateOut: Animation
) {

    private var isVisible = true

    fun start() {

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    if (isVisible) {
                        hideView()
                    }

                } else if (dy < 0) {
                    if (!isVisible) {
                        showView()
                    }
                }
            }
        })
    }

    private fun showView() {
        isVisible = true
        view.animation = animateIn
        view.visibility = View.VISIBLE
        view.startAnimation(animateIn)
    }

    private fun hideView() {
        isVisible = false
        view.animation = animateOut
        view.startAnimation(animateOut)
        view.visibility = View.GONE
    }
}
