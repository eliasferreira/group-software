package com.desafio.groupsoftware.ui.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.view.*
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.repository.ContactRepository
import com.desafio.groupsoftware.ui.activity.ContactDetailsActivity
import com.desafio.groupsoftware.ui.activity.ContactDetailsActivity.Companion.VIEW_HEADER_IMAGE
import com.desafio.groupsoftware.ui.activity.ContactFormActivity
import com.desafio.groupsoftware.ui.extras.USER_EXTRA_KEY
import com.softwareplace.workout.file.FileManager
import kotlinx.android.synthetic.main.user_layout.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ContactAdapter(
    val contactRepsitory: ContactRepository,
    var usersList: List<User>,
    val activity: Activity
) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.user_layout,
                parent,
                false
            ),
            contactRepsitory = this.contactRepsitory,
            activity = activity
        )
    }


    override fun getItemCount(): Int {
        return usersList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(user = usersList[position])
    }


    class ViewHolder(
        itemView: View,
        val contactRepsitory: ContactRepository,
        val activity: Activity
    ) : RecyclerView.ViewHolder(itemView) {

        /** Display user data on view */
        fun bind(user: User) {
            itemView.user_email.text = user.email
            itemView.user_username.text = user.username
            itemView.user_middle_name.text = user.middleName
            itemView.user_cpf.text = user.cpf

            contactFullDetailAction(user)

            menuAction(user)

            Glide.with(itemView.context)
                .load(user.image)
                .apply(
                    RequestOptions
                        .centerInsideTransform()
                        .skipMemoryCache(false)
                        .error(R.drawable.img_not_found)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                ).into(itemView.contact_image_profile)
        }

        private fun contactFullDetailAction(user: User) {
            itemView.setOnClickListener {
                startContactFullDescription(user)
            }
        }

        private fun startContactFullDescription(user: User) {
            val intent = Intent(activity, ContactDetailsActivity::class.java)
            intent.putExtra(USER_EXTRA_KEY, user)
            val activityOptionsCompat =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    Pair<View, String>(itemView.contact_image_profile, VIEW_HEADER_IMAGE)
                )
            ActivityCompat.startActivity(activity, intent, activityOptionsCompat.toBundle())

        }

        /** Item view menu action */
        private fun menuAction(user: User) {
            val userMenuAction = itemView.user_action_menu
            userMenuAction.setOnClickListener {

                val menuBuilder = MenuBuilder(itemView.context)
                val menuInflater = MenuInflater(itemView.context)
                menuInflater.inflate(R.menu.contact_menu, menuBuilder)

                val menuPopupHelper =
                    MenuPopupHelper(itemView.context, menuBuilder, userMenuAction)

                menuPopupHelper.setForceShowIcon(true)
                menuPopupHelper.show()

                onMenuItemSelected(menuBuilder, user)
            }
        }

        private fun onMenuItemSelected(
            menuBuilder: MenuBuilder,
            user: User
        ) {
            menuBuilder.setCallback(object : MenuBuilder.Callback {
                override fun onMenuItemSelected(menu: MenuBuilder, item: MenuItem): Boolean {
                    return when (item.itemId) {
                        R.id.edit_contact -> {
                            startContactUpdateAction(user)
                            true
                        }
                        R.id.contact_delete
                        -> {
                            userDialogDeleteConfirmation(user)
                            return true
                        }
                        else -> false
                    }
                }

                override fun onMenuModeChange(menu: MenuBuilder) {}
            })
        }

        private fun startContactUpdateAction(user: User) {
            contactRepsitory.user = user
            val intent =
                Intent(itemView.context, ContactFormActivity::class.java)
            itemView.context.startActivity(intent)
        }

        /** Request user deletation confirmation */
        private fun userDialogDeleteConfirmation(user: User) {
            val builder = AlertDialog.Builder(itemView.context)
            builder.setMessage("Remove ${user.username} from your contact list?")
                .setPositiveButton(R.string.fire) { _, _ ->
                    contactRepsitory.deleteContact(user)
                    deleteContactImage(user = user)
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
            builder.create()
            builder.show()
        }


        /** Delete the contact image from the device */
        private fun deleteContactImage(user: User) {
            user.image?.let { userImage ->
                FileManager(itemView.context).deleteImage(
                    userImage
                )
            }
        }
    }
}