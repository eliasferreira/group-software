package com.softwareplace.workout.ui.permission

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.desafio.groupsoftware.ui.extras.PICK_IMAGE
import com.desafio.groupsoftware.ui.extras.REQUEST_PHONE_CALL


fun callPhoneRequestUserPermission(activity: Activity, phoneNumber: String?) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE),REQUEST_PHONE_CALL);
    } else {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phoneNumber"))
        activity.startActivity(intent)
    }
}

fun imageFromCamera(activity: Activity) {
    try {
        if (cameraPermissionDenid(activity)) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(
                    Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), PICK_IMAGE
            )
        } else {
            startImagePickerIntentCamera(activity)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

}

private fun cameraPermissionDenid(activity: Activity) =
    ContextCompat.checkSelfPermission(
        activity,
        Manifest.permission.CAMERA
    ) == PackageManager.PERMISSION_DENIED

private fun startImagePickerIntentCamera(activity: Activity) {
    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE_SECURE)
    activity.startActivityForResult(takePicture, PICK_IMAGE)
}

