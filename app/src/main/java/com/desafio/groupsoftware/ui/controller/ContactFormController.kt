package com.desafio.groupsoftware.ui.controller

import android.graphics.Bitmap
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.app.BaseApplication
import com.desafio.groupsoftware.model.Phone
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.ui.activity.ContactFormActivity
import com.desafio.groupsoftware.ui.adapter.UserPhoneAdapter
import com.softwareplace.workout.file.FileManager
import com.softwareplace.workout.ui.permission.imageFromCamera
import kotlinx.android.synthetic.main.activity_user_form.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactFormController(val contactFormActivity: ContactFormActivity) {

    private var user: User? = null
    private val baseApplication: BaseApplication =
        contactFormActivity.application as BaseApplication
    private val userRepository = baseApplication.getUserRepository()
    private var imageBitmap: Bitmap? = null
    private var phoneList = ArrayList<Phone>()
    private lateinit var userPhoneAdapter: UserPhoneAdapter

    private val contactCpf = contactFormActivity.form_contact_cpf
    private val contactName = contactFormActivity.form_contact_name
    private val contactEmail = contactFormActivity.form_contact_email
    private val contactMiddleName = contactFormActivity.contact_middle_name
    private var contactImageOlde: String? = null


    init {

        onImagePickerFABAction()
        newContactOrUpdate()
        addUserPhoneNumber()
        createNewContactAction()
        findUserPhones()
    }

    /** Will call the camera intent on contactFormActivity.contact_image_select get clicked */
    private fun onImagePickerFABAction() {
        contactFormActivity.contact_image_select.setOnClickListener {
            imagePicker()
        }
    }

    /** If the form is valid, the contact will be saved into database */
    private fun createNewContactAction() {
        contactFormActivity.contact_register_button.setOnClickListener {
            if (formValidation()) {

                updateUserData()

                if (imageBitmap != null) {
                    user?.image =
                        FileManager(contactFormActivity).writeImage(bitmap = imageBitmap!!)
                            .absolutePath
                }

                user?.let { userData ->
                    userRepository.saveContact(
                        contact = userData,
                        userPhones = phoneList
                    )
                }
                userRepository.deleteContactPhones(userPhoneAdapter.phoneToBeDeleted)
                deleUserImageOld()
                contactFormActivity.finish()
            }
        }
    }

    /** Method that will delete de contact old image profile */
    private fun deleUserImageOld() {
        contactImageOlde?.let { userImage ->
            FileManager(contactFormActivity).deleteImage(
                userImage
            )
        }
    }

    /** If user from repository is null, will create a new user,
     * otherwise will update this user values*/
    private fun updateUserData() = if (user == null) {
        user = User(
            id = null,
            image = null,
            phone = null,
            email = contactEmail.text.toString().trim(),
            middleName = contactMiddleName.text.toString().trim(),
            username = contactName.text.toString().trim(),
            cpf = contactCpf.text.toString().trim()
        )
    } else {
        user?.email = contactEmail.text.toString().trim()
        user?.cpf = contactCpf.text.toString().trim()
        user?.middleName = contactMiddleName.text.toString().trim()
        user?.username = contactName.text.toString().trim()
    }


    /** Display thse list of added phone numbers */
    private fun contactPhoneListRecyclerViewStart() {
        userPhoneAdapter = UserPhoneAdapter(contactFormActivity, phoneList, false)
        contactFormActivity.recycler_view_contact_phones.apply {
            layoutManager = LinearLayoutManager(contactFormActivity, RecyclerView.VERTICAL, false)
            adapter = userPhoneAdapter
        }
    }

    /** Find contact phones */
    private fun findUserPhones() {
        CoroutineScope(Dispatchers.IO).launch {
            if (user?.id != null) {
                phoneList = userRepository.getContactPhones(user?.id!!) as ArrayList<Phone>
                contactPhoneListRecyclerViewStart()
            }
        }
    }

    /** Check if form is valid */
    private fun formValidation(): Boolean {
        if (contactName.text.isNullOrEmpty()) {
            setError(contactName)
            return false
        } else {
            contactName.error = null
        }

        if (contactMiddleName.text.isNullOrEmpty()) {
            setError(contactMiddleName)
            return false
        } else {
            contactMiddleName.error = null
        }

        if (contactEmail.text.isNullOrEmpty()) {
            setError(contactEmail)
            return false
        } else {
            contactEmail.error = null
        }

        if (contactCpf.text.isNullOrEmpty()) {
            setError(contactCpf)
            return false
        } else {
            contactCpf.error = null
        }
        return true
    }

    /** Applay a  error message a requeste an editext focus */
    private fun setError(view: EditText) {
        view.apply {
            error = contactFormActivity.getString(R.string.required_field)
            view.requestFocus()
        }
    }

    /** Retrive the phone number from the editext phone */
    private fun addUserPhoneNumber() {
        contactFormActivity.contact_add_phone.setOnClickListener {
            if (contactFormActivity.form_contact_phone_number.text.toString().trim().isNotEmpty()) {
                phoneList.add(
                    Phone(
                        id = null,
                        modality = contactFormActivity.spiner_phone_modality.selectedItem.toString(),
                        number = contactFormActivity.form_contact_phone_number.text.toString(),
                        userId = null
                    )
                )
                contactFormActivity.form_contact_phone_number.text = null
                contactPhoneListRecyclerViewStart()
            }
        }
    }

    /**@Method just to check if it's an update or creating a new contact */
    private fun newContactOrUpdate() {
        user = userRepository.user
        if (user != null) {
            contactImageOlde = user?.image
            contactName.setText(user?.username)
            contactEmail.setText(user?.email)
            contactCpf.setText(user?.cpf)
            contactMiddleName.setText(user?.middleName)
            contactFormActivity.contact_register_button.text =
                contactFormActivity.getString(R.string.update)
            contactFormActivity.supportActionBar?.title =
                contactFormActivity.getString(R.string.update_contact)
            user?.image?.let { displayImage(it) }
        }
    }

    /** Before leave, remove repository contact data, set is as null again */
    fun removeUserUpdate() {
        userRepository.user = null
    }

    /** Call the camera Intent */
    fun imagePicker() {
        imageFromCamera(contactFormActivity)
    }


    /** Display the contact image*/
    fun displayImage(url: Any) {
        Glide.with(contactFormActivity)
            .load(url)
            .apply(
                RequestOptions
                    .centerInsideTransform()
                    .skipMemoryCache(false)
                    .error(R.drawable.img_not_found)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
            ).into(contactFormActivity.contact_image_profile)
    }


    fun userImageUri(imageBitmap: Bitmap) {
        this.imageBitmap = imageBitmap
        displayImage(imageBitmap)
    }
}