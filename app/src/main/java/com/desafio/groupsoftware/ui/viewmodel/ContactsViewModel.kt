package com.desafio.groupsoftware.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.repository.ContactRepository

class ContactsViewModel : ViewModel() {

    private var users: LiveData<List<User>> = MutableLiveData<List<User>>()


    fun getUser() = users

    fun start(contactRepository: ContactRepository) {
        if (users.value == null) {
            users = contactRepository.getContacts()
        }
    }
}