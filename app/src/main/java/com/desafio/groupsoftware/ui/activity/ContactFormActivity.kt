package com.desafio.groupsoftware.ui.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.ui.controller.ContactFormController


class ContactFormActivity : AppCompatActivity() {

    private lateinit var contactFormController: ContactFormController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_form)

        contactFormController = ContactFormController(contactFormActivity = this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        contactFormController.removeUserUpdate()
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /** Handler of camera photo result*/
        if (resultCode == RESULT_OK) {
            val extras = data?.extras
            val imageBitmap = extras?.get("data") as Bitmap

            imageBitmap.let { contactFormController.userImageUri(it) }
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {

                /** If the user permission granted, will call camera intent */
                if (grantResults.isNotEmpty() && grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                    contactFormController.imagePicker()
                }
                return
            }
        }
    }
}
