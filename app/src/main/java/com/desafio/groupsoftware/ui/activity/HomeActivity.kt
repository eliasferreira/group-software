package com.desafio.groupsoftware.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.desafio.groupsoftware.R
import com.desafio.groupsoftware.ui.controller.HomeActivityController

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        HomeActivityController(homeActivity = this)
    }
}
