package com.desafio.groupsoftware.retrofit

import android.content.Context
import com.desafio.groupsoftware.R
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer(val contex: Context) {

    val retrofitUsersBaseUrlBuilder: Retrofit = Retrofit.Builder()
        .baseUrl(contex.getString(R.string.base_url))
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    val retrofitCpfBaseUrlBuilder: Retrofit = Retrofit.Builder()
        .baseUrl(contex.getString(R.string.base_url_cpf_generator))
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}