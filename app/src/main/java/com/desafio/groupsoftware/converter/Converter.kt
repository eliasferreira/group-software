package com.desafio.groupsoftware.converter

import com.desafio.groupsoftware.model.Phone
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import java.lang.reflect.Type

import androidx.room.TypeConverter

class Converter {

    @TypeConverter
    fun fromCountryLangList(countryLang: List<Phone>?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Phone>>() {

        }.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toCountryLangList(countryLangString: String?): List<Phone>? {
        if (countryLangString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Phone>>() {

        }.type
        return gson.fromJson<List<Phone>>(countryLangString, type)
    }
}
