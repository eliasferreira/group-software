package com.desafio.groupsoftware.componet

import com.desafio.groupsoftware.module.ContactRepositoryModule
import com.desafio.groupsoftware.repository.ContactRepository
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ContactRepositoryModule::class])
interface ContactRepositoryComponet {

    fun create(): ContactRepository
}