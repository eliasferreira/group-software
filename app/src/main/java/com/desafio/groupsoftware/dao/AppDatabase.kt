package com.desafio.groupsoftware.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.desafio.groupsoftware.model.Phone
import com.desafio.groupsoftware.model.User

@Database(entities = [User::class, Phone::class], version = 1,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): ContactDao
    abstract fun phoneDao(): PhoneDao
}