package com.desafio.groupsoftware.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.desafio.groupsoftware.model.User

@Dao
interface ContactDao {

    @Query("SELECT * FROM user")
    fun getAll(): List<User>

    @Insert(onConflict = REPLACE)
    fun save(user: User): Long

    @Insert(onConflict = REPLACE)
    fun save(users: List<User>)

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM user WHERE id = :id LIMIT 1")
    fun findContatc(id: Long): User
}