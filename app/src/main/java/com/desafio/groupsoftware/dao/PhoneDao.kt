package com.desafio.groupsoftware.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.desafio.groupsoftware.model.Phone
import com.desafio.groupsoftware.model.User

@Dao
interface PhoneDao {

    @Query("SELECT * FROM phone WHERE user_id = :id")
    fun getAllFromUserId(id: Long): List<Phone>

    @Insert(onConflict = REPLACE)
    fun save(phone: Phone)

    @Insert(onConflict = REPLACE)
    fun insertAll(phones: List<Phone>)

    @Delete
    fun delete(phone: Phone)

    @Delete
    fun delete(phones: List<Phone>)
}