package com.desafio.groupsoftware.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Phone(

    @PrimaryKey
    val id: Int?,

    @ColumnInfo(name = "user_id")
    var userId: Long?,
    @ColumnInfo(name = "modality")
    var modality: String,
    @ColumnInfo(name = "number")
    var number: String?
): Comparable<Phone> {
    override fun compareTo(other: Phone): Int {
        return modality.compareTo(other = other.modality)
    }
}
