package com.desafio.groupsoftware.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class User(

    @PrimaryKey
    val id: Long?,

    @ColumnInfo(name = "username")
    var username: String,

    @ColumnInfo(name = "middle_name")
    var middleName: String?,

    @ColumnInfo(name = "email")
    var email: String,

    @ColumnInfo(name = "phone")
    var phone: String?,

    @ColumnInfo(name = "cpf")
    var cpf: String?,

    @ColumnInfo(name = "image")
    var image: String?
): Comparable<User>, Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun compareTo(other: User): Int {
        return username.compareTo(other = other.username)
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(username)
        parcel.writeString(middleName)
        parcel.writeString(email)
        parcel.writeString(phone)
        parcel.writeString(cpf)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}

