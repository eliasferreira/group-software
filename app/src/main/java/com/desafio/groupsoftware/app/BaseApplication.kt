package com.desafio.groupsoftware.app

import android.app.Application
import androidx.room.Room
import com.desafio.groupsoftware.componet.ContactRepositoryComponet
import com.desafio.groupsoftware.componet.DaggerContactRepositoryComponet
import com.desafio.groupsoftware.dao.AppDatabase
import com.desafio.groupsoftware.module.ContactRepositoryModule
import com.desafio.groupsoftware.repository.ContactRepository
import com.desafio.groupsoftware.retrofit.RetrofitInitializer
import retrofit2.Retrofit

class BaseApplication : Application() {

    private lateinit var database: AppDatabase
    private lateinit var retrofitUsersBaseUrl: Retrofit
    private lateinit var retrofitCpfBaseUrl: Retrofit
    private lateinit var contactRepository: ContactRepository

    private val contactRepositoryComponet: ContactRepositoryComponet by lazy {
        DaggerContactRepositoryComponet.builder()
            .contactRepositoryModule(ContactRepositoryModule(baseApplication = this))
            .build()
    }


    override fun onCreate() {
        super.onCreate()
        retrofitCreate()
         database = Room.databaseBuilder(
            this,
            AppDatabase::class.java, "contact_database"
        ).build()
        contactRepository = contactRepositoryComponet.create()
    }

    private fun retrofitCreate() {
        retrofitUsersBaseUrl = RetrofitInitializer(contex = this).retrofitUsersBaseUrlBuilder
        retrofitCpfBaseUrl = RetrofitInitializer(contex = this).retrofitCpfBaseUrlBuilder
    }

    fun getDatabase() = this.database
    fun getRetrofitUserBaseUrl() = this.retrofitUsersBaseUrl
    fun getRetrofitCpfBaseUrl() = this.retrofitCpfBaseUrl
    fun getUserRepository() = this.contactRepository
}