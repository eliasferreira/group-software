package com.desafio.groupsoftware.module

import com.desafio.groupsoftware.app.BaseApplication
import dagger.Module
import dagger.Provides
import javax.inject.Inject

@Module
class ContactRepositoryModule @Inject constructor(
    val baseApplication: BaseApplication
) {
    @Provides
    fun getApplication() = baseApplication
}