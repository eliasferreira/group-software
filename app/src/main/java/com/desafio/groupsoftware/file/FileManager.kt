package com.softwareplace.workout.file

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class FileManager(val context: Context) {

    /** Delete a image file */
    @Throws(IOException::class)
    fun deleteImage(fullImagePath: String) {
        val file = File(fullImagePath)
        file.delete()
    }



    /** Save an image into the application directory */
    @Throws(IOException::class)
    fun writeImage(bitmap: Bitmap): File {
        var imageFile = createImageFile()

        var outputStream = FileOutputStream(imageFile)
        var bitmapFromUri = bitmap
        bitmapFromUri.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        outputStream.flush()
        outputStream.close()
        return imageFile
    }


    /** Create a file image into the application directory */
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val directory = baseFileDirectoryPath()
        if (!directory.mkdirs()) {
            Log.e("ErrorCreateDirectory", "Directory not created")
        }

        var imageFile = File(directory, "${System.currentTimeMillis()}.jpg")
        imageFile.createNewFile()
        return imageFile
    }

    private fun baseFileDirectoryPath(): File {
        return File(
            context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES
            ), USER_BASE_DIRECTORY
        )
    }

    companion object {
        const val USER_BASE_DIRECTORY = "/contact/profile/"
    }

}