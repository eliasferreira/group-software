package com.desafio.groupsoftware.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.desafio.groupsoftware.app.BaseApplication
import com.desafio.groupsoftware.model.Phone
import com.desafio.groupsoftware.model.User
import com.desafio.groupsoftware.service.CpfService
import com.desafio.groupsoftware.service.UserService
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.lang.Exception
import java.lang.NullPointerException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactRepository @Inject constructor(val baseApplication: BaseApplication) {


    var user: User? = null
    private val contactDao = baseApplication.getDatabase().userDao()
    private val phoneDao = baseApplication.getDatabase().phoneDao()

    private var mutableLiveDataContact = MutableLiveData<List<User>>()

    /** When called, this method will check if contains a contact
     ** into database, if contains, will apply this list, otherwise,
     * will try to request a list from the API.
     **/
    fun getContacts(): LiveData<List<User>> {
        CoroutineScope(Dispatchers.IO).launch {
            val contactList = contactDao.getAll()
            mutableLiveDataContact.postValue(contactList)

            /**
             ** Request from API if (@link #contactList} is null or empty
             **/
            if (contactList.isNullOrEmpty()) {
                val userSerive = baseApplication.getRetrofitUserBaseUrl().create(UserService::class.java)
                val request = userSerive.listUser()

                withContext(Dispatchers.Main) {
                    try {
                        val response = request.await()
                        if (response.isSuccessful && !response.body().isNullOrEmpty()) {
                            response.body()?.let { contact ->

                                ContactCpfFinder(
                                    contacts = contact,
                                    baseApplication = baseApplication
                                )
                            }
                        }
                    } catch (e: Exception) {
                        Log.e("Exception", e.message)
                        e.printStackTrace()
                    }
                }
            }
        }
        return mutableLiveDataContact
    }


    fun findContact(contactId: Long) = contactDao.findContatc(contactId)

    fun getContactPhones(contactId: Long) = phoneDao.getAllFromUserId(contactId)

    private fun saveUserPhones(userId: Long, userPhones: List<Phone>) {
        userPhones.forEach { userPhone ->
            userPhone.userId = userId
            phoneDao.save(userPhone)
        }
    }

    fun saveContact(contact: User, userPhones: List<Phone>) {
        CoroutineScope(Dispatchers.IO).launch {
            val userId = contactDao.save(user = contact)
            saveUserPhones(userId = userId, userPhones = userPhones)
            reload()
        }
    }

    fun saveContact(contacts: List<User>?) {
        CoroutineScope(Dispatchers.IO).launch {
            contacts?.let { contactDao.save(users = it); reload() }
        }
    }


    fun deleteContactPhones(phones: List<Phone>) {
        CoroutineScope(Dispatchers.IO).launch {
            phoneDao.delete(phones)

        }
    }

    fun deleteContact(user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            user.id?.let {
                val contactPhones = getContactPhones(it)
                deleteContactPhones(contactPhones)
            }
            contactDao.delete(user = user)
            reload()
        }
    }

    /** Update contact list, this will get all the contact already
     * saved int the database */
    private fun reload() {
        val contactList = contactDao.getAll()
        mutableLiveDataContact.postValue(contactList)
    }

    class ContactCpfFinder(
        val baseApplication: BaseApplication,
        var contacts: List<User>
    ) {
        /** This variable refers the count limit attempts to get a CPF */
        private val errorsLimit = 10
        private var errorsCounter = 0

        init {
            getContactsCpf(contacts.size - 1)
        }

        /** Method that will become into a loop trying to get a user CPF for each contact
         * into the contact list, when the request return an error, then will try again
         **/
        private fun getContactsCpf(count: Int) {
            if (count >= 0) {
                val cpfService = baseApplication.getRetrofitCpfBaseUrl().create(CpfService::class.java)

                CoroutineScope(Dispatchers.IO).launch {
                    val request = cpfService.getCpf("gerar_cpf")
                    withContext(Dispatchers.Main) {
                        try {
                            val response = request.await()
                            if (response.isSuccessful) {
                                contacts[count].cpf = response.body()?.let { cpf -> cpfFormat(cpf) }
                                Log.e("CPF", response.body())
                                errorsCounter = 0
                                getContactsCpf(count - 1)
                            } else {
                                requestError(count)
                            }
                        } catch (e: Exception) {
                            requestError(count)
                            Log.e("Exception", e.message)
                        }
                    }
                }
            } else {
                baseApplication.getUserRepository().saveContact(contacts = contacts)
            }
        }


        /** Method just to set a mask to CPF*/
        private fun cpfFormat(cpf: String): String {
            var formatedCpf = ""

            for (i: Int in 0 until cpf.length) {
                if (i == 3 || i == 6) formatedCpf += "."
                if (i == 9) formatedCpf += "-"
                formatedCpf += cpf[i]
            }
            return formatedCpf
        }

        /** Method just to plus error into error counter,
         * if the errors counter equals to errors Limit,
         * you will jump to the next one.
         */
        private fun requestError(count: Int) {
            if (errorsLimit == errorsCounter) {
                getContactsCpf(count - 1)
                errorsCounter = 0
                return
            }
            errorsCounter++
            getContactsCpf(count)
        }
    }
}