package com.desafio.groupsoftware.service

import com.desafio.groupsoftware.model.User
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface UserService {

    @GET("users")
    fun listUser(): Deferred<Response<List<User>>>
}