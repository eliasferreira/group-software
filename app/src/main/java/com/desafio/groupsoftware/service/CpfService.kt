package com.desafio.groupsoftware.service

import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CpfService {

    @POST("ferramentas_online.php")
    @FormUrlEncoded
    fun getCpf(@Field(value = "acao") action: String): Deferred<Response<String>>
}